package net.ethx.kafka;

import org.kohsuke.args4j.Option;

public class BaseArgs {
    @Option(name = "-broker", aliases = "-b", usage = "bootstrap broker address (host:port)", required = true)
    public String broker = "localhost:9092";

    @Option(name = "-topic", aliases = "-t", usage = "topic name")
    public String topic = "realtime.price";
}
