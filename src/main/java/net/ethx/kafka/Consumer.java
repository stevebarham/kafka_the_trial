package net.ethx.kafka;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.util.Collections;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class Consumer implements Runnable {
    private final KafkaConsumer<Integer, String> consumer;
    private final String topic;

    public Consumer(final ConsumerArgs args) {
        this.consumer = args.getKafkaConsumer();
        this.consumer.subscribe(Collections.singletonList(args.topic));
        this.topic = args.topic;
    }

    @Override
    public void run() {
        final Queue<ConsumerRecord<Integer, String>> records = new LinkedBlockingQueue<>();
        final MeasuringRunnable r = new MeasuringRunnable("Consumer", 100000, TimeUnit.SECONDS, () -> {
            if (records.isEmpty()) {
                consumer.poll(1000).records(topic).forEach(records::add);
            } else {
                records.poll();
            }
        });
        while (true) {
            r.run();
        }
    }

    public static void main(final String... argv) {
        final ConsumerArgs args = new ConsumerArgs();
        final CmdLineParser parser = new CmdLineParser(args);
        try {
            parser.parseArgument(argv);
            IntStream.range(0, args.threads).forEach(i -> new Thread(new Consumer(args), "consumer-" + i).start());
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }

    public static class ConsumerArgs extends BaseArgs {
        @Option(name = "-threads", usage = "Number of threads")
        public int threads = 1;

        @Option(name = "-commit-frequency", aliases = "-cf", usage = "Commit frequency in millis")
        public long commitFreqencyMillis = 5000;

        public KafkaConsumer<Integer, String> getKafkaConsumer() {
            final Properties properties = new Properties();
            properties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, broker);
            properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer");
            properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
            properties.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, commitFreqencyMillis);
            properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class);
            properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            return new KafkaConsumer<>(properties);
        }
    }
}
