package net.ethx.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

class MeasuringRunnable implements Runnable {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Runnable delegate;
    private final int interval;

    private final String key;
    private final TimeUnit period;

    private long then;
    private long thenCount;
    private long count;

    public MeasuringRunnable(final String key, final int interval, final TimeUnit period, final Runnable delegate) {
        this.delegate = delegate;
        this.interval = interval;
        this.key = key;
        this.period = period;

        this.then = System.nanoTime();
        this.thenCount = 0;
        this.count = 0;
    }

    @Override
    public void run() {
        delegate.run();
        count++;

        if (count % interval == 0) {
            final long now = System.nanoTime();
            final long nowCount = count;

            final int eventsPerPeriod = (int)((double) (nowCount - thenCount) / ((now - then) / (double) period.toNanos(1)));
            log.info("{} - processed {} events per {}", key, eventsPerPeriod, period);

            then = now;
            thenCount = nowCount;
        }
    }
}
