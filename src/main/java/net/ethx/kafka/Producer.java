package net.ethx.kafka;

import com.google.common.base.Strings;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class Producer implements Runnable {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final List<Integer> tids;
    private final List<String> messages;

    private final KafkaProducer<Integer, String> producer;
    private final String topic;

    private final Runnable sleeper;
    private final boolean sync;

    public Producer(final ProducerArgs args) {
        this.tids = IntStream.range(0, args.tids).mapToObj(i -> Math.abs(ThreadLocalRandom.current().nextInt())).collect(toList());
        this.messages = tids.stream().map(tid -> Strings.padEnd("tid:" + tid + ", price:" + ThreadLocalRandom.current().nextFloat(), args.bytes, '#')).collect(toList());
        this.producer = args.getKafkaProducer();
        this.topic = args.topic;
        this.sync = args.sync;

        final long millis = TimeUnit.MICROSECONDS.toMillis(args.micros);
        final int nanos = (int) TimeUnit.MICROSECONDS.toNanos(args.micros % 1000);
        if (millis == 0) {
            sleeper = () -> {
                long elapsed;
                final long startTime = System.nanoTime();
                do {
                    elapsed = System.nanoTime() - startTime;
                } while (elapsed < nanos);
            };
        } else {
            sleeper = () -> {
                try {
                    Thread.sleep(millis, nanos);
                } catch (InterruptedException ignored) {}
            };
        }
    }

    @Override
    public void run() {
        final MeasuringRunnable r = new MeasuringRunnable("Producer", 10000, TimeUnit.SECONDS, new Runnable() {
            int counter = 0;

            @Override
            public void run() {
                counter++;
                final int index = counter %= tids.size();
                final ProducerRecord<Integer, String> record = new ProducerRecord<>(topic, tids.get(index), messages.get(index));

                final Future<RecordMetadata> future = producer.send(record);
                if (sync) {
                    try {
                        future.get();
                    } catch (Exception e) {
                        log.error("Could not send message {} to Kafka", record, e);
                        System.exit(-1);
                    }
                }
            }
        });

        while (true) {
            r.run();
            sleeper.run();
        }
    }

    public static void main(final String... argv) {
        final ProducerArgs args = new ProducerArgs();
        final CmdLineParser parser = new CmdLineParser(args);
        try {
            parser.parseArgument(argv);
            IntStream.range(0, args.threads).forEach(i -> new Thread(new Producer(args), "producer-" + i).start());
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }

    public static class ProducerArgs extends BaseArgs {
        @Option(name = "-tids", usage = "Number of tids to generate")
        public int tids = 30000;

        @Option(name = "-tickrate", aliases = "-tr", usage = "Time between ticks in us")
        public int micros = 100;

        @Option(name = "-data", usage = "Data volume in bytes")
        public int bytes = 32;

        @Option(name = "-threads", usage = "Number of threads")
        public int threads = 1;

        @Option(name = "-sync", usage = "Wait for message delivery to complete")
        public boolean sync = false;

        public KafkaProducer<Integer, String> getKafkaProducer() {
            final Properties properties = new Properties();
            properties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, broker);
            properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
            properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

            return new KafkaProducer<>(properties);
        }
    }
}
